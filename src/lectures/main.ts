// ============================================================= TYPES =====================================================================
//  ------------- Primitive type --------------
let a: number = 1;
let b: string = "Abc";
let c: boolean = true;

//  ------------- Literal type --------------
let count: 1;
let adam: 'hoang';
let changingString = "Hello World";

//  ------------- Type Aliases --------------
type MissingNo = 404;
type SanitizalInput = string;

//  ------------- Union type --------------
type Size = "small" | "medium" | "large"; 

//  ------------- Object literal type --------------
type Point = {
    x: number;
    y: number;
}

//  ------------- Tuple type --------------
type Data = [
    location: Location,
    timestamp: string
];

//  ------------- Intersection type --------------
type Location2 = {x: number} & {y: number};

interface Colorful { color: string }
interface Circle { radius: number }
type ColorfulCircle = Colorful & Circle

//  ------------- Type Indexing --------------
type Person = {age: number; name: string; alive: boolean}
type Age = Person['age']

//  ------------- Type from Value --------------
const data = {}
type Data1 = typeof data;

//  ------------- Type from Func Return --------------
const createFixtures = () => {return 1;}
type Fixtures = ReturnType<typeof createFixtures>;

function test1(fixtures : Fixtures) {}

//  ------------- Type from Module --------------
// const data: import('./data').data

//  ------------- Mapped type --------------
type Artist = { name: string, bio: string }
type Subscriber<Type> = {
    [Property in keyof Type] : (newValue : Type[Property]) => void
}
type ArtistSub = Subscriber<Artist>;

//  ------------- Conditional type --------------
interface Brid { legs : 2}
interface Dog { legs : 4}
interface Ant { legs : 2}
interface Wolf { legs : 4}

type HasFourLegs<Animal> = Animal extends { legs : 4 } ? Animal : never;
type Animal = Brid | Dog | Ant | Wolf
type FourLegs = HasFourLegs<Animal>

//  ------------- Template Union type --------------
type SupportedLangs = "en" | "pt" | "zh"
type FooterLocaleIDs = "header" | "footer"

type AllLocaleIDs = `${SupportedLangs}_${FooterLocaleIDs}_id`;

//  ------------- Object Literal Syntax --------------
type JSONResponse = {
    version: number;
    payloadSize: number;
    outOfStock?: boolean;
    update: (retryTimes: number) => void;//arrrow function
    // update(retryTimes: number): void;//function
    (): JSONResponse; //Type is callable
    // [key: string]: number; //Accepts any index
    // new (s : string): JSONResponse; //Newable
    // readonly body:string;
}

// ============================================================= INTERFACE =====================================================================
//  ------------- Generics --------------
interface List<T> {
    length: number;
    [index: number] : T;
}

const numberList: List<number> = [1,2,3]
const woedList: List<string> = ['easy', 'hoang nguyen']

interface Student {
    id: number;
    name: string
}

const studentList: List<Student> = [{id: 1, name: 'Hoang'}]

//  ------------- Overloads --------------
interface Expect{
    (matcher: boolean): string;
    (matcher: string): boolean;
}

//  ------------- Get & Set --------------
interface Ruler{
    get size(): number
    set size(value: number | string)
}
const r: Ruler = {size: 1}

//  ------------- Extension via merging --------------
interface APICall {
    data: Response
}

interface APICall {
    error?: Error
}

//  ------------- Class conformance --------------
interface Syncable { sync(): void }
class Account implements Syncable{
    sync(): void {
        throw new Error("Method not implemented.");
    }
}


//---------------------------- keyof operator ----------------------------
interface Student {
    id: number
    name: string
}

type StudentKeys = keyof Student;
const keys: StudentKeys = 'id'

// ============================================================= CLASS =====================================================================
//  ------------- Generics --------------
class Box<Type> {
    contents: Type
    constructor(value: Type) {
        this.contents = value
    }
}
const stringBox = new Box(" A Package")

class Student2 {
    fullName: string;
    constructor(
      public firstName: string,
      public middleInitial: string,
      public lastName: string
    ) {
      this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
}
   
interface Person2 {
    firstName: string;
    lastName: string;
}
   
function greeter(person: Person2) {
    return "Hello, " + person.firstName + " " + person.lastName;
}
   
let user = new Student2("Jane", "M.", "User");

//  ------------- Parameter Property --------------
class Adress {
    constructor(public x: number, public y: number) {}
}

const adress = new Adress(20, 40)
adress.x;
adress.y;

//  ------------- Abstract Classes --------------
abstract class Base {
    abstract getName(): string;
    printName() {
        console.log("Hello" + this.getName());
        
    }
}

class Dog2 extends Base{
    getName(): string {
        return "GOGOGOGO"
    }
}
const d = new Dog2();
d.printName()

//  ------------- Decorators and Attributes --------------
function enumerable(value: boolean) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
      descriptor.enumerable = value;
    };
}

class Greeter {
    greeting: string;
    constructor(message: string) {
      this.greeting = message;
    }
   
    @enumerable(false)
    greet() {
      return "Hello, " + this.greeting;
    }
}

// ============================================================= Control Flow Analysis =====================================================================
//  ------------- If Statements --------------
// typeof (for primitives)
function printAll(strs: string | string[] | null) {
    if (strs && typeof strs === "object") {
        for (const s of strs) {
            console.log(s);
        }
    } else if (typeof strs === "string") {
        console.log(strs);
    } else {
      // do nothing
    }
}

// instanceof (for classes) 
function logValue(x: Date | string) {
    if (x instanceof Date) {
      console.log(x.toUTCString());
    } else {
      console.log(x.toUpperCase());                
    }
}

// Assignment
const data1 = {
    name: "Zagreus" 
}

const data2 = {
    name: "Zagreus" 
} as const //no change value in data2 (readonly)
   
// ---------------- Function Type Expressions --------------
function greeter2(fn: (a: string) => void) {
    fn("Hello, World");
}

function printToConsole(s: string) {
    console.log(s);
}

greeter2(printToConsole);

// option2
type GreetFunction = (a: string) => void;
function greeter3(fn: GreetFunction) {
    // ...
}

// ---------------- Call Signatures --------------
type DescribableFunction = {
    description: string;
    (someArg: number): boolean;
};
function doSomething(fn: DescribableFunction) {
    console.log(fn.description + " returned " + fn(6));
}

// ---------------- Call Signatures --------------
type SomeConstructor = {
    new (s: string): {};
};

function fn(ctor: SomeConstructor) {
    return new ctor("hello");
}
interface CallOrConstruct {
    new (s: string): Date;
    (n?: number): number;
}

// ---------------- Generic Functions --------------
function firstElement(arr: any[]) {
    return arr[0];
}
function firstElement2<Type>(arr: Type[]): Type | undefined {
    return arr[0];
}

